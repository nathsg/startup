module.exports = {
  siteMetadata: {
    title: `The Startup Fest 1ª Edição`,
    description: `Venha conhecer várias startups e suas propostas no evento The StartUp Fest - 1ª Edição`,
    author: `@n-th`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-material-ui`,
    {
      resolve: "gatsby-source-graphql",
      options: {
        typeName: "SWAPI",
        fieldName: "swapi",
        url: "https://startups-project-mytvsxrgeb.now.sh/",
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `The Startup Fest 1ª Edição`,
        short_name: `The Startup Fest`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
