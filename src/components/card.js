import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

import DialogVote from "./dialog"

const useStyles = makeStyles({
  card: {
		margin: 10,
  },
  media: {
    height: 140,
	},
	action: {
		display: "block",
		textAlign: "right",
	}
})

const CardStartup = ({ startups }) => {
  const classes = useStyles()

  return (
		<div className={classes.root}>
			{
				startups.map(item => (
				<Card className={classes.card}>
					<CardActionArea>
						<CardMedia
							className={classes.media}
							image={item.imageUrl}
							title= {item.name}
						/>
						<CardContent>
							<Typography gutterBottom variant="h5" component="h2">
								{item.name}
							</Typography>
							<Typography variant="body2" color="textSecondary" component="p">
								{item.description}
							</Typography>
						</CardContent>
					</CardActionArea> 
					<CardActions className={classes.action}>
						<DialogVote startup={item.name} />
					</CardActions>
				</Card>
				))
			}
		</div>
  )
}

export default CardStartup