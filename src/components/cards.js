import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useStaticQuery, graphql } from "gatsby"

import CardStartup from "./card"

const useStyles = makeStyles({
  root: {
		margin: 10,
		width: "30%",
  },
})

const Cards = () => {
	const classes = useStyles()
	
	const data = useStaticQuery(graphql`
		query MyQueryCards {
			swapi {
				allStartups {
					annualReceipt
					description
					imageUrl
					name
					segment_id
					teamCount
				}
			}
		}
	`)

	const allStartups = data.swapi.allStartups

	return (
		<div className={classes.root}>
				{console.log(allStartups)}
				<CardStartup startups={allStartups} />
		</div>
	)
}

export default Cards