import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTheme } from '@material-ui/core/styles'

import RatingStartup from "./rating"

const DialogVote = ({ startup }) => {
	const theme = useTheme()
	
  const [open, setOpen] = React.useState(false)
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        VOTE
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
				onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          Queremos sua opinião! 
				</DialogTitle>
        <DialogContent>
          <DialogContentText>
            O que está achando da startup {startup}?
						<br/>
						Por favor, avalie os seguintes pontos:
          </DialogContentText>
					<RatingStartup />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleClose} color="primary">
            Avaliar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default DialogVote