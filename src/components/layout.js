import React from "react"
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles'
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"

const useStyles = makeStyles({
  root: {
    margin: 10,
    fontFamily: "arial",
  }
})

const Layout = ({ children }) => {
  const classes = useStyles()
  
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div className={classes.root}>
      <Header siteTitle={data.site.siteMetadata.title} />
      <main>{children}</main>
      <footer>
        © {new Date().getFullYear()}, Built with love by 
        {` `}
        <a href="https://www.gitlab.com/nathsg"> N-th </a>
      </footer>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
