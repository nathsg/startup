import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import { useStaticQuery, graphql } from "gatsby"

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}))

const ListVotes = () => {
  const classes = useStyles()
	
  const data = useStaticQuery(graphql`
		query MyQueryList {
			swapi {
				allStartups {
						annualReceipt
						description
						imageUrl
						name
						segment_id
						teamCount
				}
			}
		}
  `)

	const allStartups = data.swapi.allStartups

  return (
    <List className={classes.root}>
			{
				allStartups.map(item => (
					<div>
						<ListItem alignItems="flex-start">
							<ListItemAvatar>
								<Avatar alt={item.name} src={item.imageUrl} />
							</ListItemAvatar>
							<ListItemText
								primary={item.name}
								secondary={
									<React.Fragment>
										{item.description}
									</React.Fragment>
								}
							/>
						</ListItem>
						<Divider variant="inset" component="li" />
					</div>
				))
			}
    </List>
  )
}

export default ListVotes