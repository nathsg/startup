import React from "react"
import Divider from '@material-ui/core/Divider'

import ListStartup from "./list"

const Lists = () =>{
	return(
		<div>
			<ListStartup />
			<Divider />
			<ListStartup />
			<ListStartup />
		</div>
	)
}

export default Lists