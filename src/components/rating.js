import React from 'react'
import Rating from '@material-ui/lab/Rating'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'

const labels = {
	1: 'Péssimo',
	2: 'Ruim',
	3: 'Ok',
	4: 'Bom',
	5: 'Excelente',
}

const useStyles = makeStyles({
  rating: {
		display: "flex",
  },
})

const RatingStartup = () => {
	const classes = useStyles()
	
  const [valueProposal, setValueProposal] = React.useState(2)
  const [valuePitch, setValuePitch] = React.useState(2)
  const [valueDevelopment, setValueDevelopment] = React.useState(2)
  const [hoverProposal, setHoverProposal] = React.useState(-1)
  const [hoverPitch, setHoverPitch] = React.useState(-1)
  const [hoverDevelopment, setHoverDevelopment] = React.useState(-1)

  return (
    <div>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Proposta</Typography>
				<div className={classes.rating}>
					<Rating
						name="proposal"
						value={valueProposal}
						onChange={(event, newValue) => {
							setValueProposal(newValue);
						}}
						onChangeActive={(event, newHover) => {
							setHoverProposal(newHover);
						}}
					/>
					{valueProposal !== null && <Box ml={2}>{labels[hoverProposal !== -1 ? hoverProposal : valueProposal]}</Box>}
				</div>
      </Box>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Apresentação</Typography>
				<div className={classes.rating}>
					<Rating
						name="pitch"
						value={valuePitch}
						onChange={(event, newValue) => {
							setValuePitch(newValue);
						}}
						onChangeActive={(event, newHover) => {
							setHoverPitch(newHover);
						}}
					/>
					{valuePitch !== null && <Box ml={2}>{labels[hoverPitch !== -1 ? hoverPitch : valuePitch]}</Box>}
				</div>
				</Box>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Desenvolvimento</Typography>
				<div className={classes.rating}>
					<Rating
						name="development"
						value={valueDevelopment}
						onChange={(event, newValue) => {
							setValueDevelopment(newValue);
						}}
						onChangeActive={(event, newHover) => {
							setHoverDevelopment(newHover);
						}}
					/>
					{valueDevelopment !== null && <Box ml={2}>{labels[hoverDevelopment !== -1 ? hoverDevelopment : valueDevelopment]}</Box>}
				</div>
				</Box>
    </div>
  )
}

export default RatingStartup