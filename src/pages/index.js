import React from "react"
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { makeStyles, useTheme } from '@material-ui/core/styles'

import Layout from "../components/layout"
import Cards from "../components/cards"
import SEO from "../components/seo"
import Lists from "../components/lists"

const useStyles = makeStyles((props) => ({
  root: {
    display: `${true? `flex`: `block`}`,
  },
}))

const IndexPage = () => {
	const theme = useTheme()
	
  const flex = useMediaQuery(theme.breakpoints.down('sm'))
  const classes = useStyles({ flex })
	

  return(
    <Layout>
      <SEO title="Venha!" />
      {console.log(flex)}
      <div className={classes.root}>
        <Cards />
        <Lists />
      </div>
    </Layout>

  )
}

export default IndexPage
